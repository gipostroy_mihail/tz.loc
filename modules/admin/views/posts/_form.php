<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Posts */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="posts-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

    <?php  $items = ArrayHelper::map($authors,'idauthors','email');?>

    <?= $form->field($model, 'authors_idauthors')->dropDownList($items,['prompt' => 'Укажите автора записи'])?>

    <?php $status = ['0' => 'Активный','1' => 'Отключен', ];?>

    <?= $form->field($model, 'deleted')->dropDownList($status)?>

<!--    --><?//= $form->field($model, 'dt')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
