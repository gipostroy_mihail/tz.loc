<?php

namespace app\controllers;

use app\models\Authors;
use app\models\Posts;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\Pagination;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * лента всех постов блога
     *
     * @return string
     */
    public function actionIndex()
    {
        $posts = Posts::find()->joinWith('authorsIdauthors')->where('posts.deleted = 0');
        $pages = new Pagination(['defaultPageSize' => 5,'totalCount' => $posts->count()]);
        $posts = $posts->offset($pages->offset)->limit($pages->limit)->all();
        return $this->render('index', [
            'posts' => $posts,
            'pages' => $pages,
        ]);
    }

    /**
     * Пост полностью
     *
     * @return Object
     */
    public function actionPost(){

        $id = (int)Yii::$app->request->get('id');

        if(!is_int($id)){
            return $this->goHome();
        }

        $post = Posts::find()->joinWith('authorsIdauthors')
            ->where('posts.deleted = 0 and posts.idposts = '. $id)
            ->One();

        return $this->render('post', [
            'post' => $post
        ]);
    }

    /**
     * Посты конкретного автора
     */
    public function actionOnly(){
        $id = (int)Yii::$app->request->get('id');
        if(!is_int($id)){
            return $this->goHome();
        }

        $onlyPostsByThisAuthor = Posts::find()
            ->joinWith('authorsIdauthors')
            ->where('posts.deleted = 0 and posts.authors_idauthors = '. $id);
        $pages = new Pagination(['defaultPageSize' => 5,'totalCount' => $onlyPostsByThisAuthor->count()]);
        $posts = $onlyPostsByThisAuthor->offset($pages->offset)->limit($pages->limit)->all();
        return $this->render('only', [
            'posts' => $posts,
            'pages' => $pages,
        ]);
    }
    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
