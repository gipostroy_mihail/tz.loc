<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "authors".
 *
 * @property integer $idauthors
 * @property string $email
 * @property string $password
 * @property string $lastname
 * @property string $firstname
 * @property integer $deleted
 *
 * @property Posts[] $posts
 */
class Authors extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'authors';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'password'], 'required'],
            [['deleted'], 'integer'],
            [['email'], 'string', 'max' => 100],
            [['password', 'lastname', 'firstname'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idauthors' => 'Idauthors',
            'email' => 'Email',
            'password' => 'Password',
            'lastname' => 'Lastname',
            'firstname' => 'Firstname',
            'deleted' => 'Deleted',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosts()
    {
        return $this->hasMany(Posts::className(), ['authors_idauthors' => 'idauthors']);
    }
}
