<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "posts".
 *
 * @property integer $idposts
 * @property string $title
 * @property string $description
 * @property string $text
 * @property integer $authors_idauthors
 * @property integer $deleted
 * @property string $dt
 *
 * @property Authors $authorsIdauthors
 */
class Posts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'posts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'description', 'text', 'authors_idauthors'], 'required'],
            [['description', 'text'], 'string'],
            [['authors_idauthors', 'deleted'], 'integer'],
            //[['dt'], 'safe'],
            [['title'], 'string', 'max' => 255],
            [['authors_idauthors'], 'exist', 'skipOnError' => true, 'targetClass' => Authors::className(), 'targetAttribute' => ['authors_idauthors' => 'idauthors']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idposts' => 'Idposts',
            'title' => 'Title',
            'description' => 'Description',
            'text' => 'Text',
            'authors_idauthors' => 'Authors Idauthors',
            'deleted' => 'Deleted',
            'dt' => 'Dt',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthorsIdauthors()
    {
        return $this->hasOne(Authors::className(), ['idauthors' => 'authors_idauthors']);
    }
}
