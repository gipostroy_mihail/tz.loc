<?php
use yii\widgets\LinkPager;
?>
<div class="main_index">

    <?php foreach($posts as $post): ?>
        <div class="main_div_index">
            <div class="title_index"><?= $post['title'];?></div>

            <div class="description_index"><?= $post['description'];?>
                <a href="<?=Yii::$app->urlManager->createUrl(['site/post','id'=>$post['idposts']])?>">читать далее...</a>
            </div>

            <div class="author_linc_index">
                <a href="<?= Yii::$app->urlManager->createUrl(['site/only','id'=>$post['authorsIdauthors']['idauthors']]);?>">
                    <?= $post['authorsIdauthors']['firstname'] .' '.$post['authorsIdauthors']['lastname'];?>
                </a>
                <?= date('Y-m-d',strtotime($post['dt']));?>
            </div>
        </div>

    <?php endforeach; ?>
    <div>
         <?= LinkPager::widget(['pagination'=>$pages]);?>
    </div>
</div>