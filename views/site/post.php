<div style="width: 90%; margin: 0 auto;">
    <h2 style="background: #e8e8e8; height: 35px; padding: 0px 0px 0px 10px;"><?= $post['title'];?></h2>
    <div style="background: #fff; padding:10px;">
        <?= $post['text'];?>
    <div>
    <div style="background: #e8e8e8; height: 25px; text-align: right; padding: 3px 10px 0px 0px;">
        <a href="<?= Yii::$app->urlManager->createUrl(['site/only','id'=>$post['authorsIdauthors']['idauthors']]);?>">
            <?= $post['authorsIdauthors']['firstname'] .' '.$post['authorsIdauthors']['lastname'];?>
        </a>
            <?= date('Y-m-d',strtotime($post['dt']));?>
    </div>
</div>
